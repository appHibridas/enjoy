import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from "../pages/login/login";
import { MapaPrincipalPage } from "../pages/mapa-principal/mapa-principal";
import { MinhasRecompensasPage } from "../pages/minhas-recompensas/minhas-recompensas";
import { MeusPontosPage } from "../pages/meus-pontos/meus-pontos";
import { MinhasExperienciasPage } from "../pages/minhas-experiencias/minhas-experiencias";
import { ConvidarAmigoPage } from "../pages/convidar-amigo/convidar-amigo";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any, icon: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      {title: 'Perfil', component: MapaPrincipalPage, icon: "person"},
      {title: 'Minhas Recompensas', component: MinhasRecompensasPage, icon: "cube"},
      {title: 'Meus Pontos', component: MeusPontosPage, icon: "card"},
      { title: 'Minhas Experiências', component: MinhasExperienciasPage , icon: "trophy"},
      { title: 'Convidar um amigo', component: ConvidarAmigoPage , icon: "people"},
      { title: 'Mapa principal', component: MapaPrincipalPage , icon: "compass"},
      { title: 'Sair', component: LoginPage , icon: "exit"},
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
