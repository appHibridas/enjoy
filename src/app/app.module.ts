import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { AngularFireModule } from 'AngularFire2';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from "../pages/login/login";
import { IntroducaoPage } from "../pages/introducao/introducao";
import { CadastrarPage } from "../pages/cadastrar/cadastrar";
import { RecuperarSenhaPage } from "../pages/recuperar-senha/recuperar-senha";
import { MapaPrincipalPage } from "../pages/mapa-principal/mapa-principal";
import { MeusPontosPage } from "../pages/meus-pontos/meus-pontos";
import { MinhasExperienciasPage } from "../pages/minhas-experiencias/minhas-experiencias";
import { MinhasRecompensasPage } from "../pages/minhas-recompensas/minhas-recompensas";
import { ConvidarAmigoPage } from "../pages/convidar-amigo/convidar-amigo";
import { FIREBASE_CONFIG } from "./app.firebase.config";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    IntroducaoPage,
    CadastrarPage,
    RecuperarSenhaPage,
    MapaPrincipalPage,
    MeusPontosPage,
    MinhasExperienciasPage,
    MinhasRecompensasPage,
    ConvidarAmigoPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    IntroducaoPage,
    CadastrarPage,
    RecuperarSenhaPage,
    MapaPrincipalPage,
    MeusPontosPage,
    MinhasExperienciasPage,
    MinhasRecompensasPage,
    ConvidarAmigoPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
