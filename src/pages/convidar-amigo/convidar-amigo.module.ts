import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConvidarAmigoPage } from './convidar-amigo';

@NgModule({
  declarations: [
    ConvidarAmigoPage,
  ],
  imports: [
    IonicPageModule.forChild(ConvidarAmigoPage),
  ],
})
export class ConvidarAmigoPageModule {}
