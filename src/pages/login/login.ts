import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , MenuController} from 'ionic-angular';
import { HomePage } from "../home/home";
import { IntroducaoPage } from "../introducao/introducao";
import { CadastrarPage } from "../cadastrar/cadastrar";
import { RecuperarSenhaPage } from "../recuperar-senha/recuperar-senha";

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private menu: MenuController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.menu.swipeEnable(false);
  }

  goToHomePage(){
    this.navCtrl.setRoot(IntroducaoPage)
  }

  goToCadastrar(){
    this.navCtrl.push(CadastrarPage)
  }
  goToRecuperarSenha(){
    this.navCtrl.push(RecuperarSenhaPage)
  }
}
