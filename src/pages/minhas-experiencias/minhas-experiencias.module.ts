import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MinhasExperienciasPage } from './minhas-experiencias';

@NgModule({
  declarations: [
    MinhasExperienciasPage,
  ],
  imports: [
    IonicPageModule.forChild(MinhasExperienciasPage),
  ],
})
export class MinhasExperienciasPageModule {}
