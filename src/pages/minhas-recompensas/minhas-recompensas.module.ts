import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MinhasRecompensasPage } from './minhas-recompensas';

@NgModule({
  declarations: [
    MinhasRecompensasPage,
  ],
  imports: [
    IonicPageModule.forChild(MinhasRecompensasPage),
  ],
})
export class MinhasRecompensasPageModule {}
